# Learning Plan Auto-complete - Moodle plugin #

With this plugin installed, all learning plans will be marked as complete when all their competencies are completed by the user.

This plugin does not have any frontend. It only listens to the \core\event\competency_evidence_created, check if all competencies are completed for all plans of the current user and completes the plans.

## License ##

2021 Daniel Neis Araujo <daniel@adapta.online>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
