<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Handle the \core\event\competency_evidence_created event.
 *
 * @package     local_lpautocomplete
 * @copyright   2021 Daniel Neis Araujo <daniel@adapta.online>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_lpautocomplete;

defined('MOODLE_INTERNAL') || die();

/**
 * Handle the \core\event\competency_evidence_created event.
 *
 * @copyright   2021 Daniel Neis Araujo <daniel@adapta.online>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class competency_observer {

    /**
     * Triggered when '\core\event\competency_evidence_created' event is triggered.
     *
     * @param \core\event\competency_evidence_created $event
     */
    public static function complete_plans(\core\event\competency_evidence_created $event) {
        global $DB;
        $ucid = $event->other['usercompetencyid'];
        $cid = $event->other['competencyid'];
        $userid = $event->relateduserid;
        $proficiency = $DB->get_field('competency_usercomp', 'proficiency', ['id' => $ucid]);
        if ($proficiency == 1) {
            $plans = $DB->get_records('competency_plan', ['userid' => $userid, 'status' => 1], '', 'id,name');
            foreach ($plans as $p) {
                $plan = \core_competency\api::read_plan($p->id);
                $pclist = \core_competency\api::list_plan_competencies($plan);
                $proficientcount = 0;
                $competencycount = 0;
                if ($data->plan->iscompleted) {
                    $ucproperty = 'usercompetencyplan';
                } else {
                    $ucproperty = 'usercompetency';
                }
                foreach ($pclist as $pc) {
                    $usercomp = $pc->$ucproperty;
                    $competencycount++;
                    if ($usercomp->get('proficiency')) {
                        $proficientcount++;
                    }
                }
                if ($competencycount == $proficientcount) {
                    \core_competency\api::complete_plan($p->id);
                }
            }
        }
    }
}
